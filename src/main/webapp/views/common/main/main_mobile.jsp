<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/main/main_head.jsp"%>	
</head>
<body>
<div class="container">
	<!-- 用户信息 -->
	<div class="row pt-3"> 
		 <div class="col-4">
		  	<c:if test='${not empty zuser.photo}'>
		  		<img class="img-fluid img-thumbnail" src="${zuser.photo}" /> 
			</c:if>
			<c:if test="${empty zuser.photo}" >
		  		<img class="img-fluid img-thumbnail" src="./img/system/touxiang.jpg" /> 
			</c:if>
		</div>
		<div class="col-8"> 
		  	<div class="col-12" ><h5>${zuser.user_name}</h5></div>
		  	<div class="col-12"><h6>${zorg.org_name}</h6></div>
		</div>
	</div>
	<hr>
	<div class="row pl-3 pr-3"> 
		<div class="col-12"> 
			<c:forEach items="${workjobList}" var="job" >
				<div class="row border mb-2"> 
					<div class="col-12" >任务：${job.workjob_title}</div>
					<div class="col-12">发起人：${job.user_name}</div>
					<div class="col-12">日期：<fmt:formatDate value="${job.create_time}" pattern="yyyy年MM月dd日HH点mm分ss秒" /></div>
					<div class="col-12 pb-2"><button type="button" class="btn btn-secondary btn-block" onclick="window.location.href='job_handle?zid=${job.workjobid}&&&biz_id=${job.biz_id}'">处理任务</button></div>
				</div>
			</c:forEach>
		  	
		</div>
	</div>

	<!-- 底部导航 -->
	<%@include file="/views/common/main/foot_mobile.jsp"%>
</div>
</body>
</html>
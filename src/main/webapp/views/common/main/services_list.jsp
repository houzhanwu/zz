<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>接口列表</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
</script>
<style type="text/css">
</style>
</head> 
<body>
<div class="container-fluid">
<div class="row">
	<div class="col-md-5 ">
		<ul class="nav nav-tabs nav-justified sticky-top" role="tablist">    
			<c:forEach items="${project_list}" var="project" varStatus="status">
				<c:if test="${status.index==0}">
			　　　	<li class="nav-item" role="presentation"><a class="nav-link text-dark active" data-toggle="tab" href="#${project.project_id}">${project.project_name}</a></li>
			　　 </c:if>  
				<c:if test="${status.index!=0}">
			　　　	<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#${project.project_id}">${project.project_name}</a></li>
			　　 </c:if> 
				
			</c:forEach>
			<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#tabletab">表单默认接口列表</a></li>
		</ul>
		<div class="tab-content mt-4 sticky-top">
			<c:forEach items="${project_list}" var="project" varStatus="status">
				<c:if test="${status.index==0}">
			　　　	<div class="tab-pane fade show active" id="${project.project_id}" role="tabpanel">
			　　 </c:if>  
				<c:if test="${status.index!=0}">
			　　　	<div class="tab-pane fade" id="${project.project_id}" role="tabpanel">
			　　 </c:if> 
					<ul class="list-group">
						<c:forEach items="${list}" var="server">
							<c:if test='${server.projectid == project.project_id}'>
		  						<li class="list-group-item"><a href="#${server.serviceid}">${server.title}</a></li>
							</c:if>
							
						</c:forEach>
					</ul>
				</div>
			</c:forEach>
			<div class="tab-pane fade show" id="tabletab" role="tabpanel">
				<ul class="list-group">
					<c:forEach items="${defaultList}" var="server">
						 <li class="list-group-item"><a href="#${server.key}">表：${server.key}</a></li>
					</c:forEach>
				</ul>
			</div>  
		</div>
	</div>
	<div class="col-md-7">
		<div class="row"><h2>定制接口列表</h2></div>
		<c:forEach items="${list}" var="server">
		<div class="row border-top border-right border-left mb-2"> 
			<div class="col-md-4 border-bottom border-right font-weight-bold" id="${server.serviceid}">${server.title}</div>
			<div class="col-md-8 border-bottom text-danger">${serverip}${server.serviceid}</div>
			<div class="col-md-4 border-right border-bottom"></div>
			<div class="col-md-8 ">
				<div class="row">
					<div class="col-md-12 border-bottom font-weight-bold">参数列表：</div>
					<div class="col-md-4 border-right border-bottom font-weight-bold">标识</div>
					<div class="col-md-4 border-right border-bottom font-weight-bold">描述</div>
					<div class="col-md-4 border-bottom font-weight-bold">类型</div>
				</div>
				<c:forEach items="${server.z_http_services_parameter_list}" var="parameter">
					<div class="row">
					<div class="col-md-4 border-right border-bottom text-success">${parameter.name}</div>
					<div class="col-md-4 border-right border-bottom">${parameter.title}</div>
					<div class="col-md-4 border-bottom"><c:out value="${parameter_type[parameter.services_parameter_type]}" /></div>
					</div>
				</c:forEach>
			</div>
		</div>
		</c:forEach>
		
		
		<div class="row "><h2>表单默认接口列表</h2></div>
		<c:forEach items="${defaultList}" var="server_map" varStatus="server_map_var_status">
		<div class="col-md-12" id="${server_map.key}"><h3>${server_map.key}</h3></div>
		<c:forEach items="${server_map.value}" var="server">
			<div class="row border-top border-right border-left mb-2"> 
				<div class="col-md-4 border-bottom border-right font-weight-bold" >${server.title}</div>
				<div class="col-md-8 border-bottom text-danger">${serverip}${server.serviceid}</div>
				<div class="col-md-4 border-right border-bottom"></div>
				<div class="col-md-8 ">
					<div class="row">
						<div class="col-md-12 border-bottom font-weight-bold">参数列表：</div>
						<div class="col-md-4 border-right border-bottom font-weight-bold">标识</div>
						<div class="col-md-4 border-right border-bottom font-weight-bold">描述</div>
						<div class="col-md-4 border-bottom font-weight-bold">类型</div>
					</div>
					<c:forEach items="${server.z_http_services_parameter_list}" var="parameter">
						<div class="row">
						<div class="col-md-4 border-right border-bottom text-success">${parameter.name}</div>
						<div class="col-md-4 border-right border-bottom">${parameter.title}</div>
						<div class="col-md-4 border-bottom"><c:out value="${parameter_type[parameter.services_parameter_type]}" /></div>
						</div>
					</c:forEach>
				</div>
			</div>
			</c:forEach>
		
		</c:forEach>
	</div>
</div>


</div>
</body>
</html>
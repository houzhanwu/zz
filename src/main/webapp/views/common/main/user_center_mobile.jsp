<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/main/main_head.jsp"%>
<script type="text/javascript">
function LoadParameter(){
	$.ajax({
		type : "GET",
		url : 'LoadParameter',
		success : function(data) {
			if(data.code=='SUCCESS'){
				alertMessager(data.msg);
			}else{
				alertErrorMessager(data.msg);
			}
		}
	});
}
</script>
</head>
<body>
<div class="container">
	<div class="row" >
  		<c:if test='${zuser.zid==sp.super_user}'>
			<div class="col-12 pb-2 pt-2"><button type="button" style="background-color: #213477;color: #fff;" class="btn btn-lg btn-block" onclick="LoadParameter();"><i class="fa fa-refresh"></i> 更新平台缓存</button></div>
			<div class="col-12 pb-2"><button type="button" style="background-color: #213477;color: #fff;" class="btn btn-lg btn-block" onclick="window.location.href='list?tableId=z_sp&return_mobile_view=user_center_mobile';"><i class="fa fa-gears"></i> 设置系统参数</button></div>
		</c:if>
		<div class="col-12 pb-2"><button type="button" style="background-color: #213477;color: #fff;" class="btn btn-lg btn-block" onclick="window.location.href='user_settings';"><i class="fa fa-sliders"></i> 修改个人信息</button></div>
  	</div>
	<!-- 底部导航 -->
	<%@include file="/views/common/main/foot_mobile.jsp"%>
</div>
</body>
</html>
<%@ page pageEncoding="utf-8"%>
<div class="container">
	<div class="row">
    	<div class="col-12 p-4 d-flex justify-content-center">
			©2018-2020 <a href="https://www.zframeworks.com/" class="z_blue" target="_blank">${sp.copyright}</a> 版权所有 | <a href="http://beian.miit.gov.cn/publish/query/indexFirst.action" class="z_blue" target="_blank">${sp.icp}</a>
		</div>
	</div>
</div>
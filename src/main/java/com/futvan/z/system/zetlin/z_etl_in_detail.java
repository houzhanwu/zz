package com.futvan.z.system.zetlin;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_etl_in_detail extends SuperBean{
	//SQL字段
	private String form_columnid;

	//存储字段
	private String to_columnid;

	/**
	* getSQL字段
	* @return form_columnid
	*/
	public String getForm_columnid() {
		return form_columnid;
  	}

	/**
	* setSQL字段
	* @return form_columnid
	*/
	public void setForm_columnid(String form_columnid) {
		this.form_columnid = form_columnid;
 	}

	/**
	* get存储字段
	* @return to_columnid
	*/
	public String getTo_columnid() {
		return to_columnid;
  	}

	/**
	* set存储字段
	* @return to_columnid
	*/
	public void setTo_columnid(String to_columnid) {
		this.to_columnid = to_columnid;
 	}

}

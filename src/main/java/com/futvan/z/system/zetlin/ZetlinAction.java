package com.futvan.z.system.zetlin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zdb.z_db_table;
import com.futvan.z.system.zetlin.ZetlinService;
@Controller
public class ZetlinAction extends SuperAction{
	@Autowired
	private ZetlinService zetlinService;
	
	/**
	 * 根据表名获取表ZID
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getZDBTableZidForTelZid")
	public @ResponseBody Result z_db_save_and_add_button(@RequestParam String zid) throws Exception {
		return zetlinService.getZDBTableZidForTelZid(zid);
	}
}

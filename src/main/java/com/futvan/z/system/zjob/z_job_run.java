package com.futvan.z.system.zjob;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_job_run extends SuperBean{
	//执行开始时间
	private String start_time;

	//执行结束时间
	private String end_time;

	//耗时【秒】
	private String times;

	//执行时间表达式
	private String jobtime;

	/**
	* get执行开始时间
	* @return start_time
	*/
	public String getStart_time() {
		return start_time;
  	}

	/**
	* set执行开始时间
	* @return start_time
	*/
	public void setStart_time(String start_time) {
		this.start_time = start_time;
 	}

	/**
	* get执行结束时间
	* @return end_time
	*/
	public String getEnd_time() {
		return end_time;
  	}

	/**
	* set执行结束时间
	* @return end_time
	*/
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
 	}

	/**
	* get耗时【秒】
	* @return times
	*/
	public String getTimes() {
		return times;
  	}

	/**
	* set耗时【秒】
	* @return times
	*/
	public void setTimes(String times) {
		this.times = times;
 	}

	/**
	* get执行时间表达式
	* @return jobtime
	*/
	public String getJobtime() {
		return jobtime;
  	}

	/**
	* set执行时间表达式
	* @return jobtime
	*/
	public void setJobtime(String jobtime) {
		this.jobtime = jobtime;
 	}

}

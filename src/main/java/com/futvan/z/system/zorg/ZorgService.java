package com.futvan.z.system.zorg;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.SystemUtil;
@Service
public class ZorgService extends SuperService{

	/**
	 * 	获取用户列表数据
	 * @param orgid
	 * @return
	 */
	public List<Map<String,String>> getUserList(String orgid) throws Exception {
		String sql = "SELECT uc.* FROM z_user uc LEFT JOIN z_org_user ud ON uc.zid = ud.userid WHERE (ud.pid != '"+orgid+"' OR ud.pid IS NULL) and uc.is_start = 1 ";
		List<Map<String,String>> userList = selectList(sql);
		return userList;
	}

	/**
	 * 	导入用户到组织 
	 * @param orgid
	 * @param string
	 */
	public void ImportUser(String orgid, String userIds) throws Exception {
		String [] userArray = userIds.split(",");
		for (int i = 0; i < userArray.length; i++) {
			//导入用户到组织下面
			String sql = "INSERT INTO z_org_user(zid,pid,userid) VALUES ('"+z.newZid("z_org_user")+"','"+orgid+"','"+userArray[i]+"')";
			insert(sql);
		}
	}
}

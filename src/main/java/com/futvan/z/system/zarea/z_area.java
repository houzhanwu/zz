package com.futvan.z.system.zarea;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_area extends SuperBean{
	//行政编号
	private String area_id;

	//完整名称
	private String merger_name;

	//名称
	private String area_name;

	//区域级别
	private String level;

	//上级地区
	private String parentid;

	//简拼
	private String Jianpin;

	/**
	* get行政编号
	* @return area_id
	*/
	public String getArea_id() {
		return area_id;
  	}

	/**
	* set行政编号
	* @return area_id
	*/
	public void setArea_id(String area_id) {
		this.area_id = area_id;
 	}

	/**
	* get完整名称
	* @return merger_name
	*/
	public String getMerger_name() {
		return merger_name;
  	}

	/**
	* set完整名称
	* @return merger_name
	*/
	public void setMerger_name(String merger_name) {
		this.merger_name = merger_name;
 	}

	/**
	* get名称
	* @return area_name
	*/
	public String getArea_name() {
		return area_name;
  	}

	/**
	* set名称
	* @return area_name
	*/
	public void setArea_name(String area_name) {
		this.area_name = area_name;
 	}

	/**
	* get区域级别
	* @return level
	*/
	public String getLevel() {
		return level;
  	}

	/**
	* set区域级别
	* @return level
	*/
	public void setLevel(String level) {
		this.level = level;
 	}

	/**
	* get上级地区
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set上级地区
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get简拼
	* @return Jianpin
	*/
	public String getJianpin() {
		return Jianpin;
  	}

	/**
	* set简拼
	* @return Jianpin
	*/
	public void setJianpin(String Jianpin) {
		this.Jianpin = Jianpin;
 	}

}

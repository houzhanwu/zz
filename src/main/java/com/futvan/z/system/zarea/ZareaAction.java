package com.futvan.z.system.zarea;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zarea.ZareaService;
@Controller
public class ZareaAction extends SuperAction{
	@Autowired
	private ZareaService zareaService;
	
	@RequestMapping(value="/getAreas")
	public @ResponseBody Result getAreas(@RequestParam HashMap<String,String> bean)  throws Exception{
		Result result = new Result();
		Result authority = z.isServiceAuthority(bean,request);
		if(Code.SUCCESS.equals(authority.getCode())){
			String zid = bean.get("zid");
			List<z_area> list = new ArrayList<z_area>();
			
			//如果zid为空，查所有地区
			if(z.isNotNull(zid)) {
				z_area a = z.areas.get(zid);
				//获取下级行政区域
				getlowerArea(a);
				list.add(a);
			}else {
				list = sqlSession.selectList("z_area_select_sql", "select * from z_area where parentid ='' or parentid is null");
				
				for (z_area a : list) {
					//获取下级行政区域
					getlowerArea(a);
				}
			}
			
			if(list.size()>0) {
				result.setCode(Code.SUCCESS);
				result.setData(list);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("未获取到行政区域信息");
			}
		}else {
			result = authority;
		}
		return result;
	}
	
	
	@RequestMapping(value="/getSimpleAreas")
	public @ResponseBody Result getSimpleAreas(@RequestParam HashMap<String,String> bean)  throws Exception{
		Result result = new Result();
		Result authority = z.isServiceAuthority(bean,request);
		if(Code.SUCCESS.equals(authority.getCode())){
			String zid = bean.get("zid");
			List<z_area_simple> list = new ArrayList<z_area_simple>();
			
			//如果zid为空，查所有地区
			if(z.isNotNull(zid)) {
				z_area aa = z.areas.get(zid);
				z_area_simple a = new z_area_simple();
				a.setZid(aa.getZid());
				a.setArea_name(aa.getArea_name());
				//获取下级行政区域
				getlowerSimpleArea(a);
				list.add(a);
			}else {
				List<HashMap<String,String>> top0areaList =  sqlSession.selectList("select", "select * from z_area where parentid ='' or parentid is null");
				//list = sqlSession.selectList("z_area_select_sql", "select * from z_area where parentid ='' or parentid is null");
				for (HashMap<String, String> hashMap : top0areaList) {
					z_area_simple a = new z_area_simple();
					a.setZid(hashMap.get("zid"));
					a.setArea_name(hashMap.get("area_name"));
					list.add(a);
				}
				
				for (z_area_simple a : list) {
					//获取下级行政区域
					getlowerSimpleArea(a);
				}
			}
			
			if(list.size()>0) {
				result.setCode(Code.SUCCESS);
				result.setData(list);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("未获取到行政区域信息");
			}
		}else {
			result = authority;
		}
		return result;
	}

	//获取下级行政区域
	private void getlowerArea(z_area a) {
		List<z_area> lowerList = z.lowerArea.get(a.getZid());
		if(z.isNotNull(lowerList) && lowerList.size()>0) {
			a.setDetail_list(lowerList);
			for (z_area la : lowerList) {
				getlowerArea(la);
			}
		}
	}
	
	private void getlowerSimpleArea(z_area_simple a) {
		List<z_area> alowerList = z.lowerArea.get(a.getZid());
		if(z.isNotNull(alowerList) && alowerList.size()>0) {
			List<z_area_simple> lowerList = new ArrayList<z_area_simple>();
			for (z_area laa : alowerList) {
				z_area_simple la = new z_area_simple();
				la.setZid(laa.getZid());
				la.setArea_name(laa.getArea_name());
				
				getlowerSimpleArea(la);
				lowerList.add(la);
			}
			a.setDetail_list(lowerList);
		}
	}
	
	
}

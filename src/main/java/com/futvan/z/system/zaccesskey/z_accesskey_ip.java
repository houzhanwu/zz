package com.futvan.z.system.zaccesskey;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_accesskey_ip extends SuperBean{
	//IP地址
	private String ip;

	/**
	* getIP地址
	* @return ip
	*/
	public String getIp() {
		return ip;
  	}

	/**
	* setIP地址
	* @return ip
	*/
	public void setIp(String ip) {
		this.ip = ip;
 	}

}

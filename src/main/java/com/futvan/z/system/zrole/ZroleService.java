package com.futvan.z.system.zrole;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.JsonUtil;
import com.futvan.z.framework.util.SystemUtil;
@Service
public class ZroleService extends SuperService{

	public List<Map<String,String>> getUserList(String roleid) throws Exception{
		String sql = "SELECT uc.* FROM z_user uc LEFT JOIN z_role_user ud ON uc.zid = ud.userid WHERE (ud.pid != '"+roleid+"' OR ud.pid IS NULL) and uc.is_start = 1 ";
		List<Map<String,String>> userList = selectList(sql);
		return userList;
	}

	public void ImportUser(String roleid, String userIds) throws Exception{
		String [] userArray = userIds.split(",");
		for (int i = 0; i < userArray.length; i++) {
			//导入用户到组织下面
			String sql = "INSERT INTO z_role_user(zid,pid,userid) VALUES ('"+z.newZid("z_role_user")+"','"+roleid+"','"+userArray[i]+"')";
			insert(sql);
		}
	}

	public List<Map<String,String>> getMenuList(String roleid) {
		String sql = "SELECT DISTINCT uc.zid,uc.seq,uc.parentid,uc.name,uc.url,'true' open,uc.formid,IF(ud.zid IS NOT NULL, 'true' ,'false' ) checked FROM z_menu uc  LEFT JOIN z_role_menu ud  ON uc.zid = ud.menuid ";
		List<Map<String,String>> menuList = selectList(sql);
		return menuList;
	}

	public void ImportMenu(String roleid, String menuIds) {
		//删除现在菜单
		String deletesql = "delete from z_role_menu where pid ='"+roleid+"'";
		delete(deletesql);
		
		//保存新的菜单
		String [] menuArray = menuIds.split(",");
		for (int i = 0; i < menuArray.length; i++) {
			//导入用户到组织下面
			String sql = "INSERT INTO z_role_menu(zid,pid,menuid) VALUES ('"+z.newZid("z_role_menu")+"','"+roleid+"','"+menuArray[i]+"')";
			insert(sql);
		}
	}
}

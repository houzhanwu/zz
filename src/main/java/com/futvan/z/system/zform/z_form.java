package com.futvan.z.system.zform;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_form extends SuperBean{
	//所属项目
	private String project_id;

	//功能ID
	private String form_id;

	//功能名称
	private String form_name;

	//数据隔离模式
	private String isolation_mode;

	//是否启动快速查询
	private String isStartQuickQuery;

	//表管理
	private List<z_form_table> z_form_table_list;

	/**
	* get所属项目
	* @return project_id
	*/
	public String getProject_id() {
		return project_id;
  	}

	/**
	* set所属项目
	* @return project_id
	*/
	public void setProject_id(String project_id) {
		this.project_id = project_id;
 	}

	/**
	* get功能ID
	* @return form_id
	*/
	public String getForm_id() {
		return form_id;
  	}

	/**
	* set功能ID
	* @return form_id
	*/
	public void setForm_id(String form_id) {
		this.form_id = form_id;
 	}

	/**
	* get功能名称
	* @return form_name
	*/
	public String getForm_name() {
		return form_name;
  	}

	/**
	* set功能名称
	* @return form_name
	*/
	public void setForm_name(String form_name) {
		this.form_name = form_name;
 	}

	/**
	* get数据隔离模式
	* @return isolation_mode
	*/
	public String getIsolation_mode() {
		return isolation_mode;
  	}

	/**
	* set数据隔离模式
	* @return isolation_mode
	*/
	public void setIsolation_mode(String isolation_mode) {
		this.isolation_mode = isolation_mode;
 	}

	/**
	* get是否启动快速查询
	* @return isStartQuickQuery
	*/
	public String getIsStartQuickQuery() {
		return isStartQuickQuery;
  	}

	/**
	* set是否启动快速查询
	* @return isStartQuickQuery
	*/
	public void setIsStartQuickQuery(String isStartQuickQuery) {
		this.isStartQuickQuery = isStartQuickQuery;
 	}

	/**
	* get表管理
	* @return 表管理
	*/
	public List<z_form_table> getZ_form_table_list() {
		return z_form_table_list;
  	}

	/**
	* set表管理
	* @return 表管理
	*/
	public void setZ_form_table_list(List<z_form_table> z_form_table_list) {
		this.z_form_table_list = z_form_table_list;
 	}

}

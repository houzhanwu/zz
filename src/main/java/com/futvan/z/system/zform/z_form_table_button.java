package com.futvan.z.system.zform;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_form_table_button extends SuperBean{
	//所属页面类型
	private String page_type;

	//方法类型
	private String function_type;

	//按钮标识
	private String button_id;

	//按钮名称
	private String button_name;

	//按钮图标
	private String button_icon;

	//是否隐藏
	private String is_hidden;

	//JS点击事件
	private String js_onclick;

	/**
	* get所属页面类型
	* @return page_type
	*/
	public String getPage_type() {
		return page_type;
  	}

	/**
	* set所属页面类型
	* @return page_type
	*/
	public void setPage_type(String page_type) {
		this.page_type = page_type;
 	}

	/**
	* get方法类型
	* @return function_type
	*/
	public String getFunction_type() {
		return function_type;
  	}

	/**
	* set方法类型
	* @return function_type
	*/
	public void setFunction_type(String function_type) {
		this.function_type = function_type;
 	}

	/**
	* get按钮标识
	* @return button_id
	*/
	public String getButton_id() {
		return button_id;
  	}

	/**
	* set按钮标识
	* @return button_id
	*/
	public void setButton_id(String button_id) {
		this.button_id = button_id;
 	}

	/**
	* get按钮名称
	* @return button_name
	*/
	public String getButton_name() {
		return button_name;
  	}

	/**
	* set按钮名称
	* @return button_name
	*/
	public void setButton_name(String button_name) {
		this.button_name = button_name;
 	}

	/**
	* get按钮图标
	* @return button_icon
	*/
	public String getButton_icon() {
		return button_icon;
  	}

	/**
	* set按钮图标
	* @return button_icon
	*/
	public void setButton_icon(String button_icon) {
		this.button_icon = button_icon;
 	}

	/**
	* get是否隐藏
	* @return is_hidden
	*/
	public String getIs_hidden() {
		return is_hidden;
  	}

	/**
	* set是否隐藏
	* @return is_hidden
	*/
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
 	}

	/**
	* getJS点击事件
	* @return js_onclick
	*/
	public String getJs_onclick() {
		return js_onclick;
  	}

	/**
	* setJS点击事件
	* @return js_onclick
	*/
	public void setJs_onclick(String js_onclick) {
		this.js_onclick = js_onclick;
 	}

}

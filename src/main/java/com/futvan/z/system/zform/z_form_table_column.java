package com.futvan.z.system.zform;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_form_table_column extends SuperBean{
	//字段类型
	private String column_type;

	//字段ID
	private String column_id;

	//字段名称
	private String column_name;

	//字段规格
	private String column_size;

	//数据库规格
	private String column_length_db;

	//列表中长度
	private String colunm_length_list;

	//多行文本高
	private String textarea_height;

	//提示信息
	private String column_help;

	//列表行链接
	private String tr_href;

	//是否非空
	private String is_null;

	//是否只读
	private String is_readonly;

	//是否隐藏
	private String is_hidden;

	//列表页隐藏
	private String is_hidden_list;

	//编辑页隐藏
	private String is_hidden_edit;

	//是否启用快速查询
	private String is_search;

	//比较符
	private String compare;

	//是否加换行
	private String isbr;

	//是否加分割线
	private String ishr;

	//排列方向
	private String arrangement_direction;

	//代码风格
	private String mode_type;

	//关联代码编号
	private String p_code_id;

	//Z5关联表
	private String z5_table;

	//Z5关联字段
	private String z5_key;

	//Z5显示字段
	private String z5_value;

	//辅助显示字段
	private String z5_value2;

	//Z5弹出页面显示字段
	private String z5_page_display;

	//Z5关联SQL
	private String z5_sql;

	//默认值类型
	private String column_default_type;

	//默认值
	private String column_default;

	/**
	* get字段类型
	* @return column_type
	*/
	public String getColumn_type() {
		return column_type;
  	}

	/**
	* set字段类型
	* @return column_type
	*/
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
 	}

	/**
	* get字段ID
	* @return column_id
	*/
	public String getColumn_id() {
		return column_id;
  	}

	/**
	* set字段ID
	* @return column_id
	*/
	public void setColumn_id(String column_id) {
		this.column_id = column_id;
 	}

	/**
	* get字段名称
	* @return column_name
	*/
	public String getColumn_name() {
		return column_name;
  	}

	/**
	* set字段名称
	* @return column_name
	*/
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
 	}

	/**
	* get字段规格
	* @return column_size
	*/
	public String getColumn_size() {
		return column_size;
  	}

	/**
	* set字段规格
	* @return column_size
	*/
	public void setColumn_size(String column_size) {
		this.column_size = column_size;
 	}

	/**
	* get数据库规格
	* @return column_length_db
	*/
	public String getColumn_length_db() {
		return column_length_db;
  	}

	/**
	* set数据库规格
	* @return column_length_db
	*/
	public void setColumn_length_db(String column_length_db) {
		this.column_length_db = column_length_db;
 	}

	/**
	* get列表中长度
	* @return colunm_length_list
	*/
	public String getColunm_length_list() {
		return colunm_length_list;
  	}

	/**
	* set列表中长度
	* @return colunm_length_list
	*/
	public void setColunm_length_list(String colunm_length_list) {
		this.colunm_length_list = colunm_length_list;
 	}

	/**
	* get多行文本高
	* @return textarea_height
	*/
	public String getTextarea_height() {
		return textarea_height;
  	}

	/**
	* set多行文本高
	* @return textarea_height
	*/
	public void setTextarea_height(String textarea_height) {
		this.textarea_height = textarea_height;
 	}

	/**
	* get提示信息
	* @return column_help
	*/
	public String getColumn_help() {
		return column_help;
  	}

	/**
	* set提示信息
	* @return column_help
	*/
	public void setColumn_help(String column_help) {
		this.column_help = column_help;
 	}

	/**
	* get列表行链接
	* @return tr_href
	*/
	public String getTr_href() {
		return tr_href;
  	}

	/**
	* set列表行链接
	* @return tr_href
	*/
	public void setTr_href(String tr_href) {
		this.tr_href = tr_href;
 	}

	/**
	* get是否非空
	* @return is_null
	*/
	public String getIs_null() {
		return is_null;
  	}

	/**
	* set是否非空
	* @return is_null
	*/
	public void setIs_null(String is_null) {
		this.is_null = is_null;
 	}

	/**
	* get是否只读
	* @return is_readonly
	*/
	public String getIs_readonly() {
		return is_readonly;
  	}

	/**
	* set是否只读
	* @return is_readonly
	*/
	public void setIs_readonly(String is_readonly) {
		this.is_readonly = is_readonly;
 	}

	/**
	* get是否隐藏
	* @return is_hidden
	*/
	public String getIs_hidden() {
		return is_hidden;
  	}

	/**
	* set是否隐藏
	* @return is_hidden
	*/
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
 	}

	/**
	* get列表页隐藏
	* @return is_hidden_list
	*/
	public String getIs_hidden_list() {
		return is_hidden_list;
  	}

	/**
	* set列表页隐藏
	* @return is_hidden_list
	*/
	public void setIs_hidden_list(String is_hidden_list) {
		this.is_hidden_list = is_hidden_list;
 	}

	/**
	* get编辑页隐藏
	* @return is_hidden_edit
	*/
	public String getIs_hidden_edit() {
		return is_hidden_edit;
  	}

	/**
	* set编辑页隐藏
	* @return is_hidden_edit
	*/
	public void setIs_hidden_edit(String is_hidden_edit) {
		this.is_hidden_edit = is_hidden_edit;
 	}

	/**
	* get是否启用快速查询
	* @return is_search
	*/
	public String getIs_search() {
		return is_search;
  	}

	/**
	* set是否启用快速查询
	* @return is_search
	*/
	public void setIs_search(String is_search) {
		this.is_search = is_search;
 	}

	/**
	* get比较符
	* @return compare
	*/
	public String getCompare() {
		return compare;
  	}

	/**
	* set比较符
	* @return compare
	*/
	public void setCompare(String compare) {
		this.compare = compare;
 	}

	/**
	* get是否加换行
	* @return isbr
	*/
	public String getIsbr() {
		return isbr;
  	}

	/**
	* set是否加换行
	* @return isbr
	*/
	public void setIsbr(String isbr) {
		this.isbr = isbr;
 	}

	/**
	* get是否加分割线
	* @return ishr
	*/
	public String getIshr() {
		return ishr;
  	}

	/**
	* set是否加分割线
	* @return ishr
	*/
	public void setIshr(String ishr) {
		this.ishr = ishr;
 	}

	/**
	* get排列方向
	* @return arrangement_direction
	*/
	public String getArrangement_direction() {
		return arrangement_direction;
  	}

	/**
	* set排列方向
	* @return arrangement_direction
	*/
	public void setArrangement_direction(String arrangement_direction) {
		this.arrangement_direction = arrangement_direction;
 	}

	/**
	* get代码风格
	* @return mode_type
	*/
	public String getMode_type() {
		return mode_type;
  	}

	/**
	* set代码风格
	* @return mode_type
	*/
	public void setMode_type(String mode_type) {
		this.mode_type = mode_type;
 	}

	/**
	* get关联代码编号
	* @return p_code_id
	*/
	public String getP_code_id() {
		return p_code_id;
  	}

	/**
	* set关联代码编号
	* @return p_code_id
	*/
	public void setP_code_id(String p_code_id) {
		this.p_code_id = p_code_id;
 	}

	/**
	* getZ5关联表
	* @return z5_table
	*/
	public String getZ5_table() {
		return z5_table;
  	}

	/**
	* setZ5关联表
	* @return z5_table
	*/
	public void setZ5_table(String z5_table) {
		this.z5_table = z5_table;
 	}

	/**
	* getZ5关联字段
	* @return z5_key
	*/
	public String getZ5_key() {
		return z5_key;
  	}

	/**
	* setZ5关联字段
	* @return z5_key
	*/
	public void setZ5_key(String z5_key) {
		this.z5_key = z5_key;
 	}

	/**
	* getZ5显示字段
	* @return z5_value
	*/
	public String getZ5_value() {
		return z5_value;
  	}

	/**
	* setZ5显示字段
	* @return z5_value
	*/
	public void setZ5_value(String z5_value) {
		this.z5_value = z5_value;
 	}

	/**
	* get辅助显示字段
	* @return z5_value2
	*/
	public String getZ5_value2() {
		return z5_value2;
  	}

	/**
	* set辅助显示字段
	* @return z5_value2
	*/
	public void setZ5_value2(String z5_value2) {
		this.z5_value2 = z5_value2;
 	}

	/**
	* getZ5弹出页面显示字段
	* @return z5_page_display
	*/
	public String getZ5_page_display() {
		return z5_page_display;
  	}

	/**
	* setZ5弹出页面显示字段
	* @return z5_page_display
	*/
	public void setZ5_page_display(String z5_page_display) {
		this.z5_page_display = z5_page_display;
 	}

	/**
	* getZ5关联SQL
	* @return z5_sql
	*/
	public String getZ5_sql() {
		return z5_sql;
  	}

	/**
	* setZ5关联SQL
	* @return z5_sql
	*/
	public void setZ5_sql(String z5_sql) {
		this.z5_sql = z5_sql;
 	}

	/**
	* get默认值类型
	* @return column_default_type
	*/
	public String getColumn_default_type() {
		return column_default_type;
  	}

	/**
	* set默认值类型
	* @return column_default_type
	*/
	public void setColumn_default_type(String column_default_type) {
		this.column_default_type = column_default_type;
 	}

	/**
	* get默认值
	* @return column_default
	*/
	public String getColumn_default() {
		return column_default;
  	}

	/**
	* set默认值
	* @return column_default
	*/
	public void setColumn_default(String column_default) {
		this.column_default = column_default;
 	}

}

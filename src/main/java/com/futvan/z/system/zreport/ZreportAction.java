package com.futvan.z.system.zreport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.system.zreport.ZreportService;
@Controller
public class ZreportAction extends SuperAction{
	@Autowired
	private ZreportService zreportService;

	/**
	 * 插入
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/ZreportInsert")
	public @ResponseBody Result ZreportInsert(@RequestParam HashMap<String,String> bean) throws Exception {
		GetSessionInfoToBean(bean, request);
		return zreportService.insertData(bean,request);
	}

	/**
	 * 修改
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/ZreportUpdate")
	public @ResponseBody Result ZreportUpdate(@RequestParam HashMap<String,String> bean) throws Exception {
		GetSessionInfoToBean(bean, request);
		return zreportService.updateData(bean,request);
	}
}

package com.futvan.z.framework.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer{

	/**
	 * WebSocket配制文件
	 */
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		WebSocketHandler wsh = new WebSocketHandler();
		registry.addHandler(wsh, "/zsocket").addInterceptors(new WebSocketInterceptor());
	}
	
}

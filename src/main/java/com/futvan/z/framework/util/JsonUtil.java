package com.futvan.z.framework.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.futvan.z.framework.core.z;
/**
 * Json工具类
 * @author 4223947@qq.com
 * @CreateDate 2018-09-27
 */
public class JsonUtil {
	
	public static void main(String[] args) {
		String s = "{\"488\":\"1\"}";
		Map<String,String> m = JsonUtil.getObject(s,Map.class);
		System.out.println(m);
	}
	
	/**
	 * JSON 转 POJO
	 */
	public static <T> T getObject(String pojo, Class<T> tclass) {
		if(z.isNotNull(pojo) && z.isNotNull(tclass)) {
			try {
				return JSONObject.parseObject(pojo, tclass);
			} catch (Exception e) {
				z.Error(tclass + "转 JSON 失败");
			}
		}else {
			z.Error("pojo and class is not null");
		}
		return null;
	}

	/**
	 * POJO 转 JSON    
	 */
	public static <T> String getJson(T tResponse){
		String pojo = JSONObject.toJSONString(tResponse);
		return pojo;
	}
	
	/**
	 * POJO 转 JSON    
	 * 标准JSON，key带双引号
	 */
	public static <T> String getJson2(T tResponse){
		String pojo = JSONObject.toJSONString(tResponse,SerializerFeature.WriteNonStringValueAsString);
		return pojo;
	}
	
	

	/**
	 * List<T> 转 json 保存到数据库
	 */
	public static <T> String listToJson(List<T> ts) {
		String jsons = JSON.toJSONString(ts);
		return jsons;
	}

	/**
	 * json 转 List<T>
	 */
	public static <T> List<T> jsonToList(String jsonString, Class<T> clazz) {
		if(!"".equals(jsonString) && jsonString!=null) {
			@SuppressWarnings("unchecked")
			List<T> ts = (List<T>) JSONArray.parseArray(jsonString, clazz);
			return ts;
		}else {
			return new ArrayList<T>();
		}
	}
	
//	/**
//	 * json 转 List<T>
//	 */
//	public static <T> List<T> jsonToList2(String jsonString, Class<T> clazz) {
//		if(!"".equals(jsonString) && jsonString!=null) {
//			@SuppressWarnings("unchecked")
//			List<T> ts = (List<T>) JSONObject.parseArray(jsonString, clazz);
//			return ts;
//		}else {
//			return new ArrayList<T>();
//		}
//	}
	
	
	
	/**
	 * json 美化
	 * @param json
	 * @return
	 */
	public static String prettyJson(String json){
		if(z.isNotNull(json)) {
			JSONObject jsonObject = null;
		    try {
		        jsonObject = JSONObject.parseObject(json,Feature.OrderedField);
		    }catch (Exception e){
		        return json;
		    }
		    return JSONObject.toJSONString(jsonObject,true);
		}else {
			return json;
		}
	}
	
}

package com.futvan.z.framework.util;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zvcode.z_vcode;

/**
 * 短信发送类
 * @author 4223947@qq.com
 *
 */
public class SmsUtil {
	/**
	 * 发送手机验证码短信
	 * @param tel
	 * @param code
	 * @return
	 */
	public static Result Send(String tel,String code) {
		//通过阿里云短信接口发送
		HashMap<String,String> map = new HashMap<String, String>();
		map.put("code", code);
		return Send("模板",tel,map);
	}

	/**
	 * 通用发送短信
	 * @param TemplateCode 短信模板
	 * @param Tel 手机号
	 * @param Param HashMap参数
	 * @return Result
	 */
	public static Result Send(String TemplateCode,String Tel,HashMap<String,String> Param) {
		Result result = new Result();
		String AccessKeyId = "XXXXXXXXXXXXXXXXXXXXXXXXXXX";
		String AccessKeySecret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
		String SignName = "XXXX";
		String TemplateParam = JsonUtil.getJson(Param);
		try {
			DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", AccessKeyId, AccessKeySecret);
			IAcsClient client = new DefaultAcsClient(profile);
			CommonRequest request = new CommonRequest();
			request.setMethod(MethodType.POST);
			request.setDomain("dysmsapi.aliyuncs.com");
			request.setVersion("2017-05-25");
			request.setAction("SendSms");
			request.putQueryParameter("RegionId", "cn-hangzhou");
			request.putQueryParameter("PhoneNumbers", Tel);
			request.putQueryParameter("SignName", SignName);
			request.putQueryParameter("TemplateCode", TemplateCode);
			request.putQueryParameter("TemplateParam", TemplateParam);
			CommonResponse response = client.getCommonResponse(request);
			Map mapdata = (Map)JsonUtil.getObject(response.getData(), Map.class);
			if ("OK".equals(mapdata.get("Code"))) {
				result.setCode(Code.SUCCESS);
				result.setMsg("发送成功");
				z.Log("发送短信成功|" + Tel + "|" + TemplateParam);
				addSendLog(Tel,"发送成功",TemplateParam);
			} else {
				result.setCode(Code.ERROR);
				result.setMsg(String.valueOf(mapdata.get("Message")));
				z.Log("发送短信失败|" + Tel + "|" + TemplateParam + "|" + String.valueOf(mapdata.get("Message")));
				addSendLog(Tel,"发送失败",String.valueOf(mapdata.get("Message"))+"|"+TemplateParam);
			}
		} catch (Exception e) {
			result.setCode(Code.ERROR);
			result.setMsg(e.getMessage());
		}
		return result;
	}

	/**
	 * 添加发送短信记录
	 * @param name
	 * @param info
	 * @param tel
	 */
	private static void addSendLog(String tel,String name,String info) {
		z_vcode vc = new z_vcode();
		vc.setZid(z.newZid("z_vcode"));
		vc.setName(name);
		vc.setVcode(info);
		vc.setTel(tel);
		vc.setEndtime(DateUtil.DateAdd(Calendar.MINUTE, 5));//当前时间加5分钟
		z.dbs.get("z").insert("z_vcode_insert", vc);
	}
}

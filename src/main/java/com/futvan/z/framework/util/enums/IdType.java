package com.futvan.z.framework.util.enums;

public enum IdType {
	sex("sex"),//性别
	birthday("x"),//生日
	area_code("area_code");//地区
	private String type;
	private IdType(String type) {
		this.type = type;
	}
}
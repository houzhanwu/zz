package com.futvan.z.framework.common.bean;

public class ProxyIP {
	private String ip;
	private int port;
	/**
	 * @return ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip 要设置的 ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port 要设置的 port
	 */
	public void setPort(int port) {
		this.port = port;
	}
}

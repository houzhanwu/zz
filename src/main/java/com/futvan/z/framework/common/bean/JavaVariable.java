package com.futvan.z.framework.common.bean;
/**
 * JAVA类变量对象
 * @author 4223947@qq.com
 */
public class JavaVariable {
	private String modifier = "private";//修饰符 
	private String prefix;//前缀
	private String type = "String";//类型
	private String name;//方法名称
	private String annotation;//注解
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
}

package com.futvan.z.framework.common.bean;
/**
 * 返回对象
 * @author 4223947@qq.com
 * @param <T>
 * @CreateDate 2018-06-08
 */
public class Result<T> {
	public Code code;//状态码
	public String msg;//信息
	public T data;//数据
	public Code getCode() {
		return code;
	}
	public void setCode(Code code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "Result [code=" + code + ", msg=" + msg + ", data=" + data + "]";
	}
	
	
}

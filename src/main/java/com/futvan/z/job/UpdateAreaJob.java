package com.futvan.z.job;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
/**
 * 更新地区表，地区全名与简拼
 * @author 42239
 *
 */
public class UpdateAreaJob implements StatefulJob {
	HashMap<String,HashMap<String,String>> m = null;

	public void execute(JobExecutionContext context) throws JobExecutionException {
		m = new HashMap<String, HashMap<String,String>>();

		SqlSession session = z.getSqlSession();
		List<HashMap<String,String>> list = session.selectList("select", "select * from z_area");
		for (HashMap<String, String> area : list) {
			m.put(area.get("zid"), area);
		}
		z.Log("需要更新："+list.size()+" 条");
		int i = 0;
		for (HashMap<String, String> area : list) {
			String merger_name = getAreaNameInfo(area);
			String Jianpin = StringUtil.getFirstSpell(area.get("area_name"));
			int num = session.update("update", "update z_area set merger_name = '"+merger_name+"',Jianpin='"+Jianpin+"' where zid = '"+area.get("zid")+"'");
			if(num!=1) {
				z.Error("更新地区表信息出错：更新数据不为1");
				break;
			}else {
				i = i+1;
				z.Log("更新成功："+i+" | "+merger_name);
			}
		}

	}

	private String getAreaNameInfo(HashMap<String, String> area) {
		String merger_name = "";
		if(z.isNotNull(area.get("parentid"))) {
			HashMap<String, String> pa = m.get(area.get("parentid"));
			merger_name = getAreaNameInfo(pa)+area.get("area_name");
		}else {
			merger_name = area.get("area_name");
		}
		return merger_name;
	}

}

package com.futvan.z.job;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.futvan.z.system.zform.z_form_table;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.EmailUtil;
import com.futvan.z.framework.util.SpringUtil;
import com.futvan.z.framework.util.SystemUtil;

public class zJob implements StatefulJob{

	public void execute(JobExecutionContext context) throws JobExecutionException {
		SystemUtil.Sleep(5000);
		z.Log("自动任务|zJob|执行时间："+DateUtil.getDateTime());
	}
}

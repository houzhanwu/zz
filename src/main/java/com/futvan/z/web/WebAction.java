package com.futvan.z.web;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.futvan.z.erp.cms_column.cms_column;
import com.futvan.z.erp.cms_info.cms_info;
import com.futvan.z.erp.cms_info.cms_info_detail;
import com.futvan.z.erp.zproduct.z_product;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.system.zform.z_form_table;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.CommonServicesUtil;
import com.futvan.z.framework.util.ImgUtil;
import com.futvan.z.framework.util.MathUtil;
import com.futvan.z.framework.util.PdfUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.util.SystemUtil;
@Controller
public class WebAction extends SuperAction {
	@Autowired
	private WebService webService;
	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/index")
	public ModelAndView index(String cityId) throws Exception {
		ModelAndView  model = new ModelAndView("web/index");
		return model;
	}


}

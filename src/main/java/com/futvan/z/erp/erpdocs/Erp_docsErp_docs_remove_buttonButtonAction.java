package com.futvan.z.erp.erpdocs;
import java.util.HashMap;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Erp_docsErp_docs_remove_buttonButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/erp_docs_remove_button")
	public @ResponseBody Result erp_docs_remove_button(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		String UserId = getUserId();
		String zids = bean.get("zids");
		String zidsListString = StringUtil.ListToString(zids, ",", "'", "'");
		
		//获取文档上传者
		int num = commonService.selectInt("SELECT COUNT(*) FROM erp_docs where zid IN ("+zidsListString+") AND create_user != '"+UserId+"'");
		if(num==0) {
			result = commonService.delete(bean,request);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("该功能只有上传者可以操作");
		}
		return result;
	}
}

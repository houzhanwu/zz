package com.futvan.z.erp.customer;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class crm_customer extends SuperBean{
	//是否启用
	private String is_enabled;

	//推荐人
	private String parentid;

	//级别
	private String level;

	//网体节点序号
	private String node;

	//昵称
	private String nickname;

	//性别
	private String sex;

	//用户名
	private String username;

	//手机号
	private String tel;

	//邮箱
	private String email;

	//生日
	private String birthday;

	//身份证号
	private String id_number;

	//登陆密码
	private String password_login;

	//微信OpenId
	private String wechat_openid;

	//照片
	private String photo;

	//学历
	private String education;

	//客户地址
	private List<crm_customer_address> crm_customer_address_list;

	/**
	* get是否启用
	* @return is_enabled
	*/
	public String getIs_enabled() {
		return is_enabled;
  	}

	/**
	* set是否启用
	* @return is_enabled
	*/
	public void setIs_enabled(String is_enabled) {
		this.is_enabled = is_enabled;
 	}

	/**
	* get推荐人
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set推荐人
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get级别
	* @return level
	*/
	public String getLevel() {
		return level;
  	}

	/**
	* set级别
	* @return level
	*/
	public void setLevel(String level) {
		this.level = level;
 	}

	/**
	* get网体节点序号
	* @return node
	*/
	public String getNode() {
		return node;
  	}

	/**
	* set网体节点序号
	* @return node
	*/
	public void setNode(String node) {
		this.node = node;
 	}

	/**
	* get昵称
	* @return nickname
	*/
	public String getNickname() {
		return nickname;
  	}

	/**
	* set昵称
	* @return nickname
	*/
	public void setNickname(String nickname) {
		this.nickname = nickname;
 	}

	/**
	* get性别
	* @return sex
	*/
	public String getSex() {
		return sex;
  	}

	/**
	* set性别
	* @return sex
	*/
	public void setSex(String sex) {
		this.sex = sex;
 	}

	/**
	* get用户名
	* @return username
	*/
	public String getUsername() {
		return username;
  	}

	/**
	* set用户名
	* @return username
	*/
	public void setUsername(String username) {
		this.username = username;
 	}

	/**
	* get手机号
	* @return tel
	*/
	public String getTel() {
		return tel;
  	}

	/**
	* set手机号
	* @return tel
	*/
	public void setTel(String tel) {
		this.tel = tel;
 	}

	/**
	* get邮箱
	* @return email
	*/
	public String getEmail() {
		return email;
  	}

	/**
	* set邮箱
	* @return email
	*/
	public void setEmail(String email) {
		this.email = email;
 	}

	/**
	* get生日
	* @return birthday
	*/
	public String getBirthday() {
		return birthday;
  	}

	/**
	* set生日
	* @return birthday
	*/
	public void setBirthday(String birthday) {
		this.birthday = birthday;
 	}

	/**
	* get身份证号
	* @return id_number
	*/
	public String getId_number() {
		return id_number;
  	}

	/**
	* set身份证号
	* @return id_number
	*/
	public void setId_number(String id_number) {
		this.id_number = id_number;
 	}

	/**
	* get登陆密码
	* @return password_login
	*/
	public String getPassword_login() {
		return password_login;
  	}

	/**
	* set登陆密码
	* @return password_login
	*/
	public void setPassword_login(String password_login) {
		this.password_login = password_login;
 	}

	/**
	* get微信OpenId
	* @return wechat_openid
	*/
	public String getWechat_openid() {
		return wechat_openid;
  	}

	/**
	* set微信OpenId
	* @return wechat_openid
	*/
	public void setWechat_openid(String wechat_openid) {
		this.wechat_openid = wechat_openid;
 	}

	/**
	* get照片
	* @return photo
	*/
	public String getPhoto() {
		return photo;
  	}

	/**
	* set照片
	* @return photo
	*/
	public void setPhoto(String photo) {
		this.photo = photo;
 	}

	/**
	* get学历
	* @return education
	*/
	public String getEducation() {
		return education;
  	}

	/**
	* set学历
	* @return education
	*/
	public void setEducation(String education) {
		this.education = education;
 	}

	/**
	* get客户地址
	* @return 客户地址
	*/
	public List<crm_customer_address> getCrm_customer_address_list() {
		return crm_customer_address_list;
  	}

	/**
	* set客户地址
	* @return 客户地址
	*/
	public void setCrm_customer_address_list(List<crm_customer_address> crm_customer_address_list) {
		this.crm_customer_address_list = crm_customer_address_list;
 	}

}
